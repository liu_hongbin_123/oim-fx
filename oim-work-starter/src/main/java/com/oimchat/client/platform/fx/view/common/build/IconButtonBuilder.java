
package com.oimchat.client.platform.fx.view.common.build;

import com.onlyxiahui.app.view.fx.component.icon.font.FontAwesomeBrandsIconButton;
import com.onlyxiahui.app.view.fx.component.icon.font.FontAwesomeRegularIconButton;
import com.onlyxiahui.app.view.fx.component.icon.font.FontAwesomeSolidIconButton;
import com.onlyxiahui.app.view.fx.component.icon.font.MaterialIconButton;

import javafx.geometry.Insets;
import javafx.scene.control.Tooltip;

/**
 * Description <br>
 * Date 2021-03-27 12:09:56<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class IconButtonBuilder {
	public static MaterialIconButton materialIconButton(String text, String tip) {
		MaterialIconButton mi = new MaterialIconButton();
		mi.setIconFontSize(23.5);
		mi.setPadding(new Insets(0, 0, 0, 0));
		mi.setOpacity(0.6);
		mi.setFontIcon(text);
		if (null != tip) {
			mi.setTooltip(new Tooltip(tip));
		}
		return mi;
	}

	public static MaterialIconButton materialIconButton(String text) {
		return materialIconButton(text, null);
	}

	public static FontAwesomeBrandsIconButton awesomeBrandsIconButton(String text, String tip) {
		FontAwesomeBrandsIconButton mi = new FontAwesomeBrandsIconButton();
		mi.setIconFontSize(19);
		mi.setPadding(new Insets(0, 0, 0, 0));
		mi.setOpacity(0.6);
		mi.setFontIcon(text);
		if (null != tip) {
			mi.setTooltip(new Tooltip(tip));
		}
		return mi;
	}

	public static FontAwesomeBrandsIconButton awesomeBrandsIconButton(String text) {
		return awesomeBrandsIconButton(text, null);
	}

	public static FontAwesomeSolidIconButton awesomeSolidIconButton(String text, String tip) {
		FontAwesomeSolidIconButton mi = new FontAwesomeSolidIconButton();
		mi.setIconFontSize(19);
		mi.setPadding(new Insets(0, 0, 0, 0));
		mi.setOpacity(0.6);
		mi.setFontIcon(text);
		if (null != tip) {
			mi.setTooltip(new Tooltip(tip));
		}
		return mi;
	}

	public static FontAwesomeSolidIconButton awesomeSolidIconButton(String text) {
		return awesomeSolidIconButton(text, null);
	}

	public static FontAwesomeRegularIconButton awesomeRegularIconButton(String text, String tip) {
		FontAwesomeRegularIconButton mi = new FontAwesomeRegularIconButton();
		mi.setIconFontSize(19);
		mi.setPadding(new Insets(0, 0, 0, 0));
		mi.setOpacity(0.6);
		mi.setFontIcon(text);
		if (null != tip) {
			mi.setTooltip(new Tooltip(tip));
		}
		return mi;
	}

	public static FontAwesomeRegularIconButton awesomeRegularIconButton(String text) {
		return awesomeRegularIconButton(text, null);
	}
}
